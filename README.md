This project was built and tested using the QT framework 5.10.1, QTcharts. It has also been tested on Gentoo using QT framework 5.15.1 and QTCharts.

I don't take any responsibility if I messed something up.

The state data included is what I believe to be the Edison data from the US 2020 presidential election. The application loads the files and allows the user to see graphs of various analytics. It should also provide any developers a convenient starting point for adding other analysis.

Build Instructions:
Windows 7
- Install Qt 5.10.1 or beyond with QtCharts
- Build within QTCreator run qmake plus makefile

