#include "mainwindow.h"

#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QStandardPaths>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <QLabel>


MainWindow::MainWindow()
{
    QHBoxLayout * qhbMainLayout = new QHBoxLayout;

    QVBoxLayout * qvbControls = new QVBoxLayout;

    btnLoadEdisonData = new QPushButton("Load Edison Data");
    connect(btnLoadEdisonData, &QPushButton::clicked, this, &MainWindow::btnLoadEdisonData_click);
    qvbControls->addWidget(btnLoadEdisonData);

    btnShowTotalVotes = new QPushButton("Show Totals");
    connect(btnShowTotalVotes, &QPushButton::clicked, this, &MainWindow::btnShowTotalVotes_click);
    qvbControls->addWidget(btnShowTotalVotes);

    btnShowTVsBCount = new QPushButton("Trump Vs Biden Total");
    connect(btnShowTVsBCount, &QPushButton::clicked, this, &MainWindow::btnShowTVsBCount_click);
    qvbControls->addWidget(btnShowTVsBCount);

    btnShowDelta = new QPushButton("Show Delta");
    connect(btnShowDelta, &QPushButton::clicked, this, &MainWindow::btnShowDelta_click);
    qvbControls->addWidget(btnShowDelta);

    btnShowTrumpNegativeCount = new QPushButton("Show Trump Negative Count");
    connect(btnShowTrumpNegativeCount, &QPushButton::clicked, this, &MainWindow::btnShowTrumpNegativeCount_click);
    qvbControls->addWidget(btnShowTrumpNegativeCount);

    btnShowBidenNegativeCount = new QPushButton("Show Biden Negative Count");
    connect(btnShowBidenNegativeCount, &QPushButton::clicked, this, &MainWindow::btnShowBidenNegativeCount_click);
    qvbControls->addWidget(btnShowBidenNegativeCount);

    btnShowNegativeCount = new QPushButton("Show Negative Count");
    connect(btnShowNegativeCount, &QPushButton::clicked, this, &MainWindow::btnShowNegativeCount_click);
    qvbControls->addWidget(btnShowNegativeCount);

    btnShowNegativeDelta = new QPushButton("Show Negative Delta");
    connect(btnShowNegativeDelta, &QPushButton::clicked, this, &MainWindow::btnShowNegativeDelta_click);
    qvbControls->addWidget(btnShowNegativeDelta);

    btnShowNewVoteRatio = new QPushButton("Show New Vote Ratio");
    connect(btnShowNewVoteRatio, &QPushButton::clicked, this, &MainWindow::btnShowNewVoteRatio_click);
    qvbControls->addWidget(btnShowNewVoteRatio);

    btnShowTVsDRatio = new QPushButton("Show T Vs D Ratio");
    connect(btnShowTVsDRatio, &QPushButton::clicked, this, &MainWindow::btnShowTVsDRatio_click);
    qvbControls->addWidget(btnShowTVsDRatio);

    qhbMainLayout->addLayout(qvbControls);

    QVBoxLayout * qvbData = new QVBoxLayout;

    lblActiveFile = new QLabel("No Open File");
    qvbData->addWidget(lblActiveFile);

    chartData= new QtCharts::QChart;

    chartData->legend()->show();

   chartViewData = new QtCharts::QChartView(chartData);
   qvbData->addWidget(chartViewData);

   txtData = new QTextEdit;

   qvbData->addWidget(txtData);

   qhbMainLayout->addLayout(qvbData);

    QWidget * wgtMain = new QWidget;
    wgtMain->setLayout(qhbMainLayout);

    setCentralWidget(wgtMain);
}

void MainWindow::btnLoadEdisonData_click()
{

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Edison File"),
                                                    QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + "/Fraud/",
                                                    tr("JSON (*.json);;All (*)"));
    if (fileName.isEmpty())
    {
        return;
    }

    QFile loadFile(fileName);

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return;
    }

     vecSnapShots.clear();

    lblActiveFile->setText(fileName.split("/").last());

    QByteArray saveData = loadFile.readAll();

    //["data"]["races"][0]["timeseries"]
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonObject  json = loadDoc.object();

    if (json.contains("data"))
    {
        json = json["data"].toObject();

        printf("found data!\n");
    }
    else
    {
        printf("I didn't see any data :(\n");
        return;
    }

    if (json.contains("races"))
    {
        json = json["races"].toArray()[0].toObject();

        printf("we got the presidential race!\n");
    }
    else{
        printf("we missed the race :(\n");
        return;
    }

    if (json.contains("timeseries")) {
        QJsonArray snapShotArray = json["timeseries"].toArray();

        printf("I found a time seires with %d elements!\n", snapShotArray.size());

        for (int index = 0; index < snapShotArray.size(); index ++) {
            QJsonObject snapShotObject = snapShotArray[index].toObject();
            QJsonObject ratioObject = snapShotObject["vote_shares"].toObject();

            snapShot newSnapShot;

            newSnapShot.totalVotes = snapShotObject["votes"].toInt();
            newSnapShot.trumpRatio = ratioObject["trumpd"].toDouble();
            newSnapShot.bidenRatio = ratioObject["bidenj"].toDouble();
            newSnapShot.trumpCount = static_cast<int>(newSnapShot.trumpRatio * newSnapShot.totalVotes);
            newSnapShot.bidenCount = static_cast<int>(newSnapShot.bidenRatio * newSnapShot.totalVotes);

            QString timeStamp = snapShotObject["timestamp"].toString();
            newSnapShot.timeStamp = QDateTime::fromString(timeStamp, Qt::ISODate);

            if (index > 0)
            {
                newSnapShot.trumpDelta = newSnapShot.trumpCount - vecSnapShots.last().trumpCount;
                newSnapShot.bidenDelta = newSnapShot.bidenCount - vecSnapShots.last().bidenCount;

                if (newSnapShot.trumpDelta < 0)
                {
                    newSnapShot.trumpTotalNegativeDelta = newSnapShot.trumpDelta + vecSnapShots.last().trumpTotalNegativeDelta;
                }
                else
                {
                    newSnapShot.trumpTotalNegativeDelta = vecSnapShots.last().trumpTotalNegativeDelta;
                }

                if (newSnapShot.bidenDelta < 0)
                {
                    newSnapShot.bidenTotalNegativeDelta = newSnapShot.bidenDelta + vecSnapShots.last().bidenTotalNegativeDelta;
                }
                else
                {
                    newSnapShot.bidenTotalNegativeDelta = vecSnapShots.last().bidenTotalNegativeDelta;
                }

                int totalDelta = newSnapShot.totalVotes - vecSnapShots.last().totalVotes;
                if (totalDelta != 0)
                {
                    newSnapShot.trumpRatioNewBallots = static_cast<qreal>(newSnapShot.trumpDelta) / totalDelta;
                    newSnapShot.bidenRatioNewBallots = static_cast<qreal>(newSnapShot.bidenDelta)/ totalDelta;
                }
                else
                {
                    newSnapShot.trumpRatioNewBallots = 0.5;
                    newSnapShot.bidenRatioNewBallots = 0.5;
                }

                if (newSnapShot.bidenDelta != 0)
                {
                    newSnapShot.trumpVsBidenRatioNewBallots = static_cast<qreal>(newSnapShot.trumpDelta) / newSnapShot.bidenDelta;

                }
                else
                {
                    newSnapShot.trumpVsBidenRatioNewBallots = 1;
                }

            }
            else
            {
                newSnapShot.trumpDelta = 0;
                newSnapShot.bidenDelta = 0;


                newSnapShot.trumpTotalNegativeDelta = 0;
                newSnapShot.bidenTotalNegativeDelta = 0;

                newSnapShot.trumpVsBidenRatioNewBallots = 1;
                newSnapShot.trumpRatioNewBallots = 0;
                newSnapShot.bidenRatioNewBallots = 0;
            }


            vecSnapShots.append(newSnapShot);
        }
    }
    else
    {
        printf("I didn't see a time series!\n");
        return;
    }

    txtData_refresh();
    btnShowTVsBCount_click();
    /*
    for (int index = 0; index < vecSnapShots.count(); index ++)
    {
        printf("Snap shot %s votes - %d, trump ratio - %f, biden ratio - %f, trump count - %d, biden count - %d\n", qPrintable(vecSnapShots[index].timeStamp.toString(Qt::ISODate)), vecSnapShots[index].totalVotes, vecSnapShots[index].trumpRatio, vecSnapShots[index].bidenRatio,
               vecSnapShots[index].trumpCount, vecSnapShots[index].bidenCount);
    }
    */
}

void MainWindow::txtData_refresh()
{
    txtData->clear();

    for (int index = 0; index < vecSnapShots.count(); index ++)
    {
        txtData->append("Time Stamp: " + vecSnapShots[index].timeStamp.toString(Qt::ISODate)
                        + " Total Votes: " + QString::number(vecSnapShots[index].totalVotes)
                        + " Trump: " + QString::number(vecSnapShots[index].trumpCount)
                        + " Delta: " + QString::number(vecSnapShots[index].trumpDelta)
                        + " Biden: " + QString::number(vecSnapShots[index].bidenCount)
                        + " Delta: " + QString::number(vecSnapShots[index].bidenDelta));
    }
}

void MainWindow::btnShowTotalVotes_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }


    QtCharts::QLineSeries *series = new QtCharts::QLineSeries;
    series->setName("Total Votes");

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 0; index < vecSnapShots.count(); index ++)
    {
        series->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].totalVotes);
    }


    chartData->addSeries(series);

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setTickCount(50);
    axisX->setLabelsAngle(-90);
    axisX->setFormat("yyyy-MM-ddTHH:mm:ssZ");
    axisX->setTitleText("Time");
    chartData->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Total Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    lastAxisX = axisX;
    lastAxisY = axisY;
}

void MainWindow::btnShowTVsBCount_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }

    int min = 0;
    int max = 0;

    QtCharts::QLineSeries *seriesTrump = new QtCharts::QLineSeries;
    seriesTrump->setName("Trump Votes");
    QPen penTrump = seriesTrump->pen();
    penTrump.setWidth(1);
    penTrump.setBrush(QBrush("red")); // or just pen.setColor("red");
    seriesTrump->setPen(penTrump);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesTrump->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].trumpCount);

        if (vecSnapShots[index].trumpCount < min)
        {
            min = vecSnapShots[index].trumpCount;
        }

        if (vecSnapShots[index].trumpCount > max)
        {
            max = vecSnapShots[index].trumpCount;
        }
    }


    chartData->addSeries(seriesTrump);

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setFormat("yyyy-MM-ddTHH:mm:ssZ");
    axisX->setTitleText("Time");
    axisX->setTickCount(50);
    axisX->setLabelsAngle(-90);
    chartData->addAxis(axisX, Qt::AlignBottom);
    seriesTrump->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    seriesTrump->attachAxis(axisY);

    QtCharts::QLineSeries *seriesBiden = new QtCharts::QLineSeries;
    seriesBiden->setName("Biden Votes");
    QPen penBiden = seriesBiden->pen();
    penBiden.setWidth(1);
    penBiden.setBrush(QBrush("blue")); // or just pen.setColor("red");
    seriesBiden->setPen(penBiden);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesBiden->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].bidenCount);

        if (vecSnapShots[index].bidenCount < min)
        {
            min = vecSnapShots[index].bidenCount;
        }

        if (vecSnapShots[index].bidenCount > max)
        {
            max = vecSnapShots[index].bidenCount;
        }
    }

    chartData->addSeries(seriesBiden);

    seriesBiden->attachAxis(axisX);
    seriesBiden->attachAxis(axisY);

    axisY->setMin(min);
    axisY->setMax(max);

    lastAxisX = axisX;
    lastAxisY = axisY;


}


void MainWindow::btnShowDelta_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }

    int min = 0;
    int max = 0;

    QtCharts::QLineSeries *seriesTrump = new QtCharts::QLineSeries;
    seriesTrump->setName("Trump New Votes");
    QPen penTrump = seriesTrump->pen();
    penTrump.setWidth(1);
    penTrump.setBrush(QBrush("red")); // or just pen.setColor("red");
    seriesTrump->setPen(penTrump);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesTrump->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].trumpDelta);

        if (vecSnapShots[index].trumpDelta < min)
        {
            min = vecSnapShots[index].trumpDelta;
        }

        if (vecSnapShots[index].trumpDelta > max)
        {
            max = vecSnapShots[index].trumpDelta;
        }
    }


    chartData->addSeries(seriesTrump);

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setFormat("yyyy-MM-ddTHH:mm:ssZ");
    axisX->setTitleText("Time");
    axisX->setTickCount(50);
    axisX->setLabelsAngle(-90);
    chartData->addAxis(axisX, Qt::AlignBottom);
    seriesTrump->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    seriesTrump->attachAxis(axisY);

    QtCharts::QLineSeries *seriesBiden = new QtCharts::QLineSeries;
    seriesBiden->setName("Biden New Votes");
    QPen penBiden = seriesBiden->pen();
    penBiden.setWidth(1);
    penBiden.setBrush(QBrush("blue")); // or just pen.setColor("red");
    seriesBiden->setPen(penBiden);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesBiden->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].bidenDelta);

        if (vecSnapShots[index].bidenDelta < min)
        {
            min = vecSnapShots[index].bidenDelta;
        }

        if (vecSnapShots[index].bidenDelta > max)
        {
            max = vecSnapShots[index].bidenDelta;
        }
    }

    chartData->addSeries(seriesBiden);

    seriesBiden->attachAxis(axisX);
    seriesBiden->attachAxis(axisY);

    axisY->setMin(min);
    axisY->setMax(max);

    lastAxisX = axisX;
    lastAxisY = axisY;
}

void MainWindow::btnShowNegativeCount_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }

    int min = 0;
    int max = 0;

    QtCharts::QLineSeries *seriesTrump = new QtCharts::QLineSeries;
    seriesTrump->setName("Trump Negative Votes");
    QPen penTrump = seriesTrump->pen();
    penTrump.setWidth(1);
    penTrump.setBrush(QBrush("red")); // or just pen.setColor("red");
    seriesTrump->setPen(penTrump);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesTrump->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].trumpTotalNegativeDelta);

        if (vecSnapShots[index].trumpTotalNegativeDelta < min)
        {
            min = vecSnapShots[index].trumpTotalNegativeDelta;
        }

        if (vecSnapShots[index].trumpTotalNegativeDelta > max)
        {
            max = vecSnapShots[index].trumpTotalNegativeDelta;
        }
    }


    chartData->addSeries(seriesTrump);

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setFormat("yyyy-MM-ddTHH:mm:ssZ");
    axisX->setTitleText("Time");
    axisX->setLabelsAngle(-90);
    chartData->addAxis(axisX, Qt::AlignBottom);
    seriesTrump->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    seriesTrump->attachAxis(axisY);

    QtCharts::QLineSeries *seriesBiden = new QtCharts::QLineSeries;
    seriesBiden->setName("Biden Negative Votes");
    QPen penBiden = seriesBiden->pen();
    penBiden.setWidth(1);
    penBiden.setBrush(QBrush("blue")); // or just pen.setColor("red");
    seriesBiden->setPen(penBiden);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);

    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesBiden->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].bidenTotalNegativeDelta);

        if (vecSnapShots[index].bidenTotalNegativeDelta < min)
        {
            min = vecSnapShots[index].bidenTotalNegativeDelta;
        }

        if (vecSnapShots[index].bidenTotalNegativeDelta > max)
        {
            max = vecSnapShots[index].bidenTotalNegativeDelta;
        }
    }

    chartData->addSeries(seriesBiden);

    seriesBiden->attachAxis(axisX);
    seriesBiden->attachAxis(axisY);

    axisY->setMin(min);
    axisY->setMax(max);

    lastAxisX = axisX;
    lastAxisY = axisY;
}

void MainWindow::btnShowTrumpNegativeCount_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }


    QtCharts::QLineSeries *seriesTrump = new QtCharts::QLineSeries;
    seriesTrump->setName("Trump Negative Votes");
    QPen penTrump = seriesTrump->pen();
    penTrump.setWidth(1);
    penTrump.setBrush(QBrush("red")); // or just pen.setColor("red");
    seriesTrump->setPen(penTrump);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesTrump->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].trumpTotalNegativeDelta);
    }


    chartData->addSeries(seriesTrump);

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setFormat("yyyy-MM-ddTHH:mm:ssZ");
    axisX->setTitleText("Time");
    axisX->setLabelsAngle(-90);
    chartData->addAxis(axisX, Qt::AlignBottom);
    seriesTrump->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    seriesTrump->attachAxis(axisY);

    lastAxisX = axisX;
    lastAxisY = axisY;
}

void MainWindow::btnShowBidenNegativeCount_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }


    QtCharts::QLineSeries *seriesBiden = new QtCharts::QLineSeries;
    seriesBiden->setName("Biden Negative Votes");
    QPen penBiden = seriesBiden->pen();
    penBiden.setWidth(1);
    penBiden.setBrush(QBrush("blue")); // or just pen.setColor("red");
    seriesBiden->setPen(penBiden);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesBiden->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].bidenTotalNegativeDelta);
    }


    chartData->addSeries(seriesBiden);

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setFormat("yyyy-MM-ddTHH:mm:ssZ");
    axisX->setTitleText("Time");
    axisX->setLabelsAngle(-90);
    chartData->addAxis(axisX, Qt::AlignBottom);
    seriesBiden->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    seriesBiden->attachAxis(axisY);

    lastAxisX = axisX;
    lastAxisY = axisY;
}


void MainWindow::btnShowNegativeDelta_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }


    QtCharts::QBarSet * trumpSet = new QtCharts::QBarSet("Trump");
    QtCharts::QBarSet * bidenSet = new QtCharts::QBarSet("Biden");

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        if ((vecSnapShots[index].trumpDelta < 0) || (vecSnapShots[index].bidenDelta < 0))
        {
            //printf("showing negative votes at index %i\n", index);
            trumpSet->append(vecSnapShots[index].trumpDelta);
            bidenSet->append(vecSnapShots[index].bidenDelta);
        }

    }




     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    QtCharts::QBarSeries *series = new QtCharts::QBarSeries;
    series->append(trumpSet);
    series->append(bidenSet);
    series->setName("New Negative Votes");

    chartData->addSeries(series);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    lastAxisY = axisY;
}

void MainWindow::btnShowNewVoteRatio_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }

    qreal min = 0;
    qreal max = 0;

    QtCharts::QLineSeries *seriesTrump = new QtCharts::QLineSeries;
    seriesTrump->setName("Trump New Vote Ratio");
    QPen penTrump = seriesTrump->pen();
    penTrump.setWidth(1);
    penTrump.setBrush(QBrush("red")); // or just pen.setColor("red");
    seriesTrump->setPen(penTrump);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesTrump->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].trumpRatioNewBallots);

        if (vecSnapShots[index].trumpRatioNewBallots < min)
        {
            min = vecSnapShots[index].trumpRatioNewBallots;
        }

        if (vecSnapShots[index].trumpRatioNewBallots > max)
        {
            max = vecSnapShots[index].trumpRatioNewBallots;
        }
    }


    chartData->addSeries(seriesTrump);

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setFormat("yyyy-MM-ddTHH:mm:ssZ");
    axisX->setTitleText("Time");
    axisX->setLabelsAngle(-90);
    chartData->addAxis(axisX, Qt::AlignBottom);
    seriesTrump->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%f");
    axisY->setTitleText("Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    seriesTrump->attachAxis(axisY);

    QtCharts::QLineSeries *seriesBiden = new QtCharts::QLineSeries;
    seriesBiden->setName("Biden New Vote Ratio");
    QPen penBiden = seriesBiden->pen();
    penBiden.setWidth(1);
    penBiden.setBrush(QBrush("blue")); // or just pen.setColor("red");
    seriesBiden->setPen(penBiden);

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        seriesBiden->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].bidenRatioNewBallots);

        if (vecSnapShots[index].bidenRatioNewBallots < min)
        {
            min = vecSnapShots[index].bidenRatioNewBallots;
        }

        if (vecSnapShots[index].bidenRatioNewBallots > max)
        {
            max = vecSnapShots[index].bidenRatioNewBallots;
        }
    }

    chartData->addSeries(seriesBiden);

    seriesBiden->attachAxis(axisX);
    seriesBiden->attachAxis(axisY);

    axisY->setMin(min);
    axisY->setMax(max);

    lastAxisX = axisX;
    lastAxisY = axisY;
}

void MainWindow::btnShowTVsDRatio_click()
{
    chartData->removeAllSeries();
    if (lastAxisX != nullptr)
    {
        chartData->removeAxis(lastAxisX);
    }

    if (lastAxisY != nullptr)
    {
        chartData->removeAxis(lastAxisY);
    }


    QtCharts::QLineSeries *series = new QtCharts::QLineSeries;
    series->setName("T Vs B New Vote Ratio");

     //connect(series, &QLineSeries::hovered, this, &motionProfileGraphs::series_mouseOver);


    for (int index = 1; index < vecSnapShots.count(); index ++)
    {
        series->append(vecSnapShots[index].timeStamp.toMSecsSinceEpoch(), vecSnapShots[index].trumpVsBidenRatioNewBallots);
    }


    chartData->addSeries(series);

    QDateTimeAxis *axisX = new QDateTimeAxis;
    axisX->setTickCount(10);
    axisX->setFormat("yyyy-MM-ddTHH:mm:ssZ");
    axisX->setTitleText("Time");
    chartData->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%f");
    axisY->setTitleText("Total Votes");
    chartData->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    axisY->setMin(0);
    axisY->setMax(2);

    lastAxisX = axisX;
    lastAxisY = axisY;
}


