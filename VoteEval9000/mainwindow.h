#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QDateTime>
#include <QtCharts>

class QPushButton;
class QLabel;

struct snapShot{
    int totalVotes;
    QDateTime timeStamp;

    qreal trumpRatio;
    qreal bidenRatio;

    int trumpCount;
    int bidenCount;

    int trumpDelta;
    int bidenDelta;

    int trumpTotalNegativeDelta;
    int bidenTotalNegativeDelta;

    qreal trumpVsBidenRatioNewBallots;
    qreal trumpRatioNewBallots;
    qreal bidenRatioNewBallots;

};

class MainWindow: public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();

    QVector<snapShot> vecSnapShots;

    QLabel * lblActiveFile;

    QtCharts::QChartView *chartViewData;
    QtCharts::QChart *chartData;
    QAbstractAxis * lastAxisX;
    QAbstractAxis * lastAxisY;

    QTextEdit *txtData;
    void txtData_refresh();

    QPushButton * btnLoadEdisonData;
    void btnLoadEdisonData_click();

    QPushButton * btnShowTotalVotes;
    void btnShowTotalVotes_click();

    QPushButton * btnShowTVsBCount;
    void btnShowTVsBCount_click();

    QPushButton * btnShowDelta;
    void btnShowDelta_click();

    QPushButton * btnShowNegativeCount;
    void btnShowNegativeCount_click();

    QPushButton * btnShowTrumpNegativeCount;
    void btnShowTrumpNegativeCount_click();

    QPushButton * btnShowBidenNegativeCount;
    void btnShowBidenNegativeCount_click();

    QPushButton * btnShowNegativeDelta;
    void btnShowNegativeDelta_click();

    QPushButton * btnShowNewVoteRatio;
    void btnShowNewVoteRatio_click();

    QPushButton * btnShowTVsDRatio;
    void btnShowTVsDRatio_click();
};

#endif // MAINWINDOW_H
